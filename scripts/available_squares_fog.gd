extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# TODO: make visible after dialogue for current player

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

onready var world = get_tree().get_root().get_node("World")
var current_entity
var move_distance
var cell_size
var tile
var pathfinder

var fogVisible = false
	
func set_pathfinder(pf):
	pathfinder = pf

var vars;
func set_origin(entity):
	if entity:
		vars = {
			"move_distance": entity.character.turn_limits.move_distance,
			"movement": entity.movement,
			"cell_size": entity.cell_size,
			"tile": entity.tile,
		}
		position = entity.position
		update()

func hide():
	fogVisible = false
	update()

func show():
	fogVisible = true
	update()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if vars:
		var shouldHide = (vars.movement.moving) or (world.current_turn == Game.CONTROL.AI or world.get_current_context(vars.tile) == Game.CONTEXT.NOT_PLAYABLE)
		var shouldShow = (not vars.movement.moving) and world.current_turn == Game.CONTROL.PLAYER and (world.get_current_context(vars.tile) == Game.CONTEXT.GUARD)
		if fogVisible:
			if shouldHide:
				hide()
		else:
			if shouldShow:			
				show()

func entity_at(position_vector):
	var characters = get_tree().get_nodes_in_group ("characters")
	for character in characters:
		if character.tile.x == position_vector.x and character.tile.y == position_vector.y and not character.is_dead:
			return character
	return null
	
func _draw():
	if fogVisible and vars and pathfinder:
		draw_rect(Rect2(Vector2(-100, -100) * vars.cell_size, Vector2(200, 200) * vars.cell_size), Color(0,0,0,1))
		
		var tileDim = Vector2(vars.cell_size, vars.cell_size)
		var max_range = range(-vars.move_distance + 1, vars.move_distance, 1)
		for x in max_range:
			for y in max_range:
				if not(x == 0 and y == 0):
					var space_tile = vars.tile + Vector2(x, y)
					var target = entity_at(space_tile)
					
					if not target:
						var path = pathfinder.find_path(vars.tile, space_tile)
						var path_size = path.size()
						if (path_size > 0) and (path_size <= vars.move_distance):
							draw_rect(Rect2(Vector2(x * vars.cell_size, y * vars.cell_size), tileDim), Color(1,1,1,2))
						
